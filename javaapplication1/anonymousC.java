package javaapplication1;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Amar-7488
 */
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.lang.reflect.Method;


interface sampleListener
{
    void onAdd(int ans);
}
interface sample
{
    void add(int a, int b);
}
public class anonymousC {
    public static void main(String[] args) throws IOException, ParseException
    {
        //Anonymous Class
        sample s = new sample(){
            //Listener Anonymous Class
            sampleListener sl = new sampleListener() {
                public void onAdd(int ans)
                {
                    System.out.println("Numbers added. Answer is "+ ans);
                }
            };
            
            @Override
            public void add(int a, int b)
            {
                sl.onAdd(a+b);
            }
        };
        s.add(2,3);
        FileReader fr = new FileReader("sam.json");
        Object obj = new JSONParser().parse(fr);
        JSONObject jo = (JSONObject)obj;
//        System.out.println(jo);
        
        // getting address
//        Map address = ((Map)jo.get("address"));
//         
//        // iterating address Map
//        Iterator<Map.Entry> itr1 = address.entrySet().iterator();
//        while (itr1.hasNext()) {
//            Map.Entry pair = itr1.next();
//            System.out.println(pair.getKey() + " : " + pair.getValue());
//        }
        for (Object v : jo.entrySet())
        {
            Map.Entry entry = (Map.Entry)v;
            System.out.println(entry.getKey() +"::"+ entry.getValue());
        }
        
        System.out.println("");
        
        Class c =obj.getClass();
        System.out.println(c.getName());
        Method[] m = c.getMethods();
        for(Method x:m)
        {
            System.out.println(x.getName());
        }
        System.out.println("");
        Annotation[] a = c.getAnnotations();
        for(Annotation x: a)
        {
            System.out.println(x.annotationType().getClass());
        }
        
    }
}
