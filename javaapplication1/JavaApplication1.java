/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Amar-7488
 */

interface vehicle
{
    public void displayProperties();
    
}

abstract class railways implements vehicle
{
    void samplefn()
    {
        System.out.println("From Railways Abstract Class");
    }
}

abstract class roadways implements vehicle
{
    void samplefn()
    {
        System.out.println("From Roadways Abstract Class");
    }
    
    abstract void roadProp();
}

class car extends roadways
{
    @Override
    public void displayProperties()
    {
        System.out.println("Car:\n"
                + "Has normally 4 wheels\n"
                + "Upto 4 to 10 persons can travel");
    }
    
    @Override
    void roadProp()
    {
        System.out.println("Needs a average sized road");
    }
    
    public void tryOutFn()
    {
        System.out.println("Just a sample fn");
    }
}

class bus extends roadways
{
    @Override
    public void displayProperties()
    {
        System.out.println("Bus:\n"
                + "Has 6 to 8 wheels\n"
                + "Upto 40 to 50 persons");
    }
    
    @Override
    void roadProp()
    {
        System.out.println("Needs a bigger road");
    }
    
    public void tryOutFn()
    {
        System.out.println("Just a sample fn");
    }
}

class train extends railways
{
    public void displayProperties()
    {
        System.out.println("Train:\n"
                + "Comfortable and Low Cost when compared");
    }
}
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        vehicle c = new car();
        roadways b = new bus();

        b.samplefn();
        
        b.displayProperties();
        b.roadProp();
        
        c.displayProperties();
        //c.roadProp();
        car cc = new car();
        cc.tryOutFn();
        cc.roadProp();
        
        railways t = new train();
        t.displayProperties();
        t.samplefn();
    }
}
